/** @type {import('ts-jest').JestConfigWithTsJest} */
export default {
  preset: 'ts-jest',
  testEnvironment: 'jest-environment-jsdom',
  setupFilesAfterEnv: ["./jest.setup.ts"],
  reporters: [
    "default",
    [
      "jest-html-reporters", {
        "publicPath": "C:\workspace\react-typescript-project\reports",
        "pageTitle": "Test Report",
        "filename": "TestReport.html"
      }
    ]
  ]
};