<pre>
#開発環境<br/>
■ Visual Studio Code (version:1.96.2)<br/>
・URL<br/>
 https://apps.microsoft.com/detail/xp9khm4bk9fz7q?launch=true&mode=full&hl=ja-jp&gl=jp&ocid=bingwebsearch<br/>
■ nodeJs<br/>
・URL<br/>
 https://nodejs.org/en<br/>
■ vite<br/>
・コマンド<br/>
「npm install -g vite」<br/>
</pre>

<pre>
#フォルダ構成<br/>
■ srcフォルダ配下<br/>
・内容：React+TypeScriptの実装フォルダ<br/>
<br/>
■ storiesフォルダ配下<br/>
・内容：StoryBook<br/>
      @storybook/testing-libraryと@storybook/jestによるテスト実装フォルダ<br/>
<br/>
■ testフォルダ配下<br/>
・内容：React+TypeScriptのテスト実装フォルダ<br/>
</pre>

<pre>
# 画面表示<br/>
下記、いずれかを行う。<br/>
・ npmコマンドの場合<br/>
「npx vite」コマンド実行⇒ターミナル上の「Local」リンク押下<br/>
・ Visual Studio Codeからの場合<br/>
「NPMスクリプト」⇒「package.json」⇒「dev」押下<br/>
※Visual Studio Code上から実行する
</pre>

<pre>
#テスト実行<br/>
下記、いずれかを行う。<br/>
・ npmコマンドの場合<br/>
「npx jest」コマンド実行
・ Visual Studio Codeからの場合<br/>
「NPMスクリプト」⇒「package.json」⇒「test」を押下する。
※Visual Studio Code上から実行する
</pre>
<pre>
■ StoryBook(環境構築)
下記のコマンドを実行する。
<br/>
・「npx sb@7 init」
※実行後は、「yes」を入力してstoryBookの環境構築を行う。

■ StoryBook(起動)
・下記、いずれかを行う。
<br/>
・「npm run storybook」
・「NPMスクリプト」⇒「package.json」⇒「storybook」を押下する。
</pre>

<pre>
#参考資料<br/>
#TypeScript開発において、実開発を進めていく中で役立ったサイトの紹介。

■ 参考サイト<br/>
・サバイバルTypeScript<br/>
https://typescriptbook.jp/

・JEST<br/>
https://jestjs.io/ja/docs/api

・Material UI <br/>
https://mui.com/material-ui/all-components/


■ Udemy<br/>
・TypeScriptではじめるWebアプリケーションテスト入門
</pre>

<pre>
#ライブラリー<br/>
※package.json参照

@emotion/react@11.13.3
@emotion/styled@11.13.0
@mui/material@5.16.7
@storybook/addon-essentials@7.6.20
@storybook/addon-interactions@7.6.20
@storybook/addon-links@7.6.20
@storybook/addon-onboarding@1.0.11
@storybook/blocks@7.6.20
@storybook/jest@0.2.2
@storybook/react-vite@7.6.20
@storybook/react@7.6.20
@storybook/test@7.6.20
@storybook/testing-library@0.2.1
@testing-library/jest-dom@6.6.2
@testing-library/react@16.0.1
@testing-library/user-event@14.5.2
@types/jest@29.5.12
@types/node@22.7.9
@types/react-dom@18.2.19
@types/react@18.2.55
@typescript-eslint/eslint-plugin@6.21.0
@typescript-eslint/parser@6.21.0
@vitejs/plugin-react@4.2.1
axios@1.6.7
eslint-plugin-react-hooks@4.6.0
eslint-plugin-react-refresh@0.4.5
eslint-plugin-storybook@0.10.1
eslint@8.56.0
jest-environment-jsdom@29.7.0
jest-html-reporter@3.10.2
jest@29.7.0
jsdoc@4.0.3
react-dom@18.2.0
react-router-dom@6.22.0
react@18.3.1
storybook@7.6.20
ts-jest@29.2.5
typescript@5.3.3
vite@5.1.1
</pre>

<pre>
#Visual Studio Codeの拡張機能<br/>
・Debugger for Java
・Language Support for Java(TM) by Red Hat
・Auto Close Tag
・Auto Rename Tag
・ES7+ React/Redux/React-Native snippets
・ESLint
・Extension Pack for Java
・Git Graph
・Git History
・GitLens — Git supercharged
・Gradle for Java
・IntelliCode
・IntelliCode API Usage Examples
・Japanese Language Pack for Visual Studio Code
・Live Server
・markdownlint
・Maven for Java
・Prettier - Code formatter
・Project Manager for Java
・SonarLint
・Spring Boot Dashboard
・Spring Boot Extension Pack
・Spring Boot Tools
・Spring Initializr Java Support
・Test Runner for Java
</pre>