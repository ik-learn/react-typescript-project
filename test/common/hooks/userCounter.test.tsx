import { act, renderHook } from "@testing-library/react";
import userCounter from "../../../src/common/hooks/userCounter";

describe("useCounter",() =>{
    it("increment", () => {
       // Hookの設定
       const { result } = renderHook(() => userCounter(1));
       // インクリメント前のカウント検証
       expect(result.current.count).toBe(1);
       // インクリメント
       act(() => result.current.increment());
       // インクリメント後のカウント検証
       expect(result.current.count).toBe(2);
    });

    it("decrement", () => {
        const { result } = renderHook(() => userCounter(2));
        expect(result.current.count).toBe(2);
        act(() => result.current.decrement());
        expect(result.current.count).toBe(1);
     });
});