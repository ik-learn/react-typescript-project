import {readFile}  from "../../../src/common/utils/FileUtils";

it ("ファイル読み込み(単体テスト)", () => {
    readFile("package.json")
        .then((data) => {
            const resultData = String(data);
            expect(resultData.length).toBeGreaterThan(0);
        })
        .catch((exception) => {
            fail(exception);
        });
});

it ("ファイル読み込み(単体テスト)", () => {
    readFile("package.json", "json")
        .then((data) => {
            const resultData = String(data);
            expect(resultData.length).toBeGreaterThan(0);
        })
        .catch((exception) => {
            fail(exception);
        });
});


it ("ファイル読み込み(単体テスト)", () => {
    readFile("package1.json", "json")
        .then((data) => {
            fail(data);
        })
        .catch((exception) => {
            const exceptionData = String(exception);
            expect(exceptionData.length).toBeGreaterThan(0);
        });
});

it ("ファイル読み込み(モックテスト)", () => {
    const mockFunc = jest.fn();
    mockFunc.mockImplementation(() => readFile("package.json")).mockReturnValue("test");
    expect(mockFunc()).toBe("test");
});

it ("ファイル読み込み(非同期モックテスト))", async () => {
    const mockFunc = jest.fn();
    mockFunc.mockImplementation(() => readFile("package.json")).mockResolvedValue("test");
    const result = await mockFunc();
    
    // 期待する戻り値
    const expectedReturnValue = "test";
    // 戻り値チェック
    expect(result).toBe(expectedReturnValue);
    // 関数呼び出しチェック
    expect(mockFunc).toHaveBeenCalled();
});

it ("ファイル読み込み(非同期モックテスト))", async () => {
    const mockFunc = jest.fn(() => readFile("package.json"));
    // Mock関数実行(1回目)
    mockFunc();
    // Mock関数実行(2回目)
    mockFunc();

    // 関数呼び出し回数チェック
    expect(mockFunc).toHaveBeenCalledTimes(2);
});
