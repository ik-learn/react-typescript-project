import { getWidthByTitleLength } from "../../../src/common/utils/StringUtils";

// 文字列数毎の横幅パターン
it.each`
  title           | expected
  ${""}           | ${0}
  ${"あ"}         | ${48}
  ${"あい"}       | ${66}
  ${"あいう"}     | ${84} 
  ${"あいうえ"}   | ${102} 
  ${"あいうえお"} | ${120} 
`("$titleの横幅は$expectedになる", ({title, expected})=> {
    expect(getWidthByTitleLength(title)).toBe(expected);
});