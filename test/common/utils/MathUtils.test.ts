import { summary, subtraction, multiplication} from "../../../src/common/utils/MathUtils";

// 足し算
// 単体
it ("足し算", () => {
    expect(summary(1, 2)).toBe(3);
})

// パターン
it.each`
  a         | b        | expected
  ${1}      | ${1}     | ${2}
  ${1}      | ${2}     | ${3}
  ${2}      | ${2}     | ${4}
  ${null}   | ${2}     | ${"error"} 
  ${1}      | ${null}  | ${"error"} 
`("$aと$bを足し算すると$expectedになる", ({a, b, expected})=> {
    expect(summary(a, b)).toBe(expected);
});

// 引き算
// 単体
it ("引き算", () => {
  expect(subtraction(1, 2)).toBe(-1);
})

// パターン
it.each`
a      | b        | expected
${1}   | ${1}     | ${0}
${1}   | ${2}     | ${-1}
${2}   | ${2}     | ${0}
${3}   | ${2}     | ${1}
${null}| ${2}     | ${"error"} 
${1}   | ${null}  | ${"error"} 
`("$aと$bを引くと$expectedになる", ({a, b, expected})=> {
  expect(subtraction(a, b)).toBe(expected);
});

// 掛け算
it ("掛け算(単体テスト)", () => {
  expect(multiplication(1, 2)).toBe(2);
})

it.each`
a      | b        | expected
${1}   | ${1}     | ${1}
${1}   | ${2}     | ${2}
${2}   | ${2}     | ${4}
${null}| ${2}     | ${"error"} 
${1}   | ${null}  | ${"error"}
`("$aと$bを加算すると$expectedになる", ({a, b, expected})=> {
  expect(multiplication(a, b)).toBe(expected);
});
