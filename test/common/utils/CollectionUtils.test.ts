import { find, isExistToList } from "../../../src/common/utils/CollectionUtils";

const stringList : string[] = ["a", "b", "c"];

it ("find(単体テスト)", () => {
    // 1件以上であること
    expect(find(stringList, "b")?.length).toBeGreaterThanOrEqual(1);
    // 想定結果通りであること
    expect(find(stringList, "b")).toBe("b");
});

it.each`
  a               | b          | expected
  ${stringList}   | ${"a"}     | ${"a"}
  ${stringList}   | ${"d"}     | ${undefined}
`("$aに$bが含まれるかの結果は$expectedになる", 
    (
        // a:each内にあるa列のデータ
        // b:each内にあるb列のデータ
        // expected:each内にあるexpected列のデータ
        {a, b, expected}
    )=> {
            expect(find(a, b)).toBe(expected);
        }
);

it ("inContain(単体テスト)", () => {
    // リスト内に対象が含まれること
    expect(isExistToList(stringList, "b")).toBe(true);
});

it.each`
  a               | b          | expected
  ${stringList}   | ${"a"}     | ${true}
  ${stringList}   | ${"d"}     | ${false}
`("$aに$bが含まれるかのチェック結果は$expectedになる",
    (
        // a:each内にあるa列のデータ
        // b:each内にあるb列のデータ
        // expected:each内にあるexpected列のデータ
        {a, b, expected}
    )=> {
            expect(isExistToList(a, b)).toBe(expected);
        }
 );