import { getFetch } from "../../../src/common/logic/AxiosLogic"

const funcSuccess = (result: any) => {
    return String(result);
}

const funcException = (result: any) => {
    return String(result);
}


it ("[正常] Rest通信", () => {
    getFetch("https://www.yahoo.co.jp/", 
            funcSuccess, 
            funcException)
        .then((data) => {
            const resultData = String(data);
            expect(resultData.length).toBeGreaterThan(0);
            expect(resultData).not.toEqual(undefined);
        })
        .catch((exception) => {
            fail(exception);
        });
});

it ("[異常] Rest通信(存在しないURL)", () => {
    getFetch("https://www.yah00.co.jp/",
        funcSuccess, 
        funcException
    )
    // 通信正常時
    .then((data) => {
        fail(data);
    })
    // 通信異常時
    .catch((exception) => {
        const exceptionData = String(exception);
        expect(exceptionData.length).toBeGreaterThan(0);
    });
});


it ("ファイル読み込み(非同期モックテスト))", async () => {
    const mockFunc = jest.fn();
    mockFunc.mockImplementation(
        () => getFetch("https://www.yahoo.co.jp/", 
                        funcSuccess, 
                        funcException)).mockResolvedValue("test");
    const result = await mockFunc();
    
    // 期待する戻り値
    const expectedReturnValue = "test";
    // 戻り値チェック
    expect(result).toBe(expectedReturnValue);
    // 関数呼び出しチェック
    expect(mockFunc).toHaveBeenCalled();
});