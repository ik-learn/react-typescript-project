import { render, screen } from "@testing-library/react";
import Button from "../../../src/common/parts/Button";

/**
 * ボタン描画のテストコード
 */
describe("Button",() =>{
    it("Buttonタグが描画される", () => {

        // ボタン描画
        render(
            <Button
                label="test" 
                onClick={()=>alert("test")}
            />
        );

        // ボタン要素取得
        const btnElement = screen.getByRole("button");
        // ボタン描画検証
        expect(btnElement).toBeInTheDocument();
        // ボタンラベル名検証
        expect(btnElement).toHaveTextContent("test");
    })
});