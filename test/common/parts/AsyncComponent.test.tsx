import { render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import AsyncComponent from "../../../src/common/parts/AsyncComponent";

const user = userEvent.setup();

describe("AsyncComponent",() =>{
    it("ボタンをクリックすると非同期処理が実行される", async () => {

        // AsyncComponentの描画
        render(<AsyncComponent/>);

        // 
        const input = screen.getByText("Initial text");
        expect(input).toBeInTheDocument();

        // ボタン要素取得
        const button = screen.getByRole("button");
        // ボタン押下
        await user.click(button);
        // ボタン押下直後の検証
        expect(screen.getByText("Loading...")).toBeInTheDocument();
        await waitFor(() => {
            expect(screen.getByText("Updated text")).toBeInTheDocument();
        },{
            // 時間間隔
            interval: 50,
            // タイムアウト時間
            timeout: 3000
        });
    });
});