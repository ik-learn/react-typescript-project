import axios  from "axios";
import { render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { UserSearch } from "../../../src/common/parts/UserSearch";

const user = userEvent.setup();

jest.mock("axios");

// axiosのMock設定
const mockAxios = jest.mocked(axios);

describe("UserSearch", () => {

    // jest実行前処理
    beforeEach(() => {
        // axiosのmock化リセット
        mockAxios.get.mockReset();
    })

    it("入力フォームに入力した内容でAPIリクエストが送信される", async ()=>{

        // APIリスクストの応答結果Mock化
        const userInfo = {
            id: 1,
            name: "Taro"
        };
        const mockResp = { data: userInfo };
        mockAxios.get.mockResolvedValue(mockResp);

        // UserSearchコンポーネントの描画
        render(<UserSearch/>);

        // テキスト入力
        const inputElement = screen.getByRole("textbox");
        await user.type(inputElement, userInfo.name);

        // ボタン押下
        const buttonElement = screen.getByRole("button");
        await user.click(buttonElement);

        // 想定通りのAPIリクエストが送信されたかの検証
        expect(mockAxios.get).toHaveBeenCalledWith(
            `/api/users?query=${userInfo.name}`
        );
    });

    it("APIから取得したユーザー結果が画面表示される", async ()=>{

        // APIリスクストの応答結果Mock化
        const userInfo = {
            id: 1,
            name: "Taro"
        };
        const mockResp = { data: userInfo };
        mockAxios.get.mockResolvedValue(mockResp);

        // UserSearchコンポーネントの描画
        render(<UserSearch/>);

        // テキスト入力
        const inputElement = screen.getByRole("textbox");
        await user.type(inputElement, userInfo.name);

        // ボタン押下
        const buttonElement = screen.getByRole("button");
        await user.click(buttonElement);

        // 検索結果
        await waitFor(() => expect(screen.getByText(userInfo.name)).toBeInTheDocument());
    });
});