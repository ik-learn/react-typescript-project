import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

import Form from "../../../src/common/parts/Form";


const user = userEvent.setup();

describe("Form",() =>{
    it("初期状態はテキストは空欄であること", () => {
        render(
            <Form/>
        );
        const input = screen.getByPlaceholderText("Enter text");
        // レンダーチェック
        expect(input).toBeInTheDocument();
        // ラベル名チェック
        expect(input).toHaveTextContent("");
    });

    it("入力したテキストがSubmitされること", async () => {

        const alertSpy = jest.spyOn(window, "alert").mockReturnValue();

        render(
            <Form/>
        );
        // テキストコンポーネント取得
        const input = screen.getByPlaceholderText("Enter text");
        // テキストコンポーネントへの入力イベント
        await user.type(input, "Test Text");
        // テキストコンポーネントへの入力検証
        expect(screen.getByDisplayValue("Test Text")).toBeInTheDocument();

        // ボタンクリック
        const button = screen.getByRole("button");
        await user.click(button);
        // アラート表示の検証
        expect(alertSpy).toHaveBeenCalledWith("submitted: Test Text");
        // Spyのクリア
        alertSpy.mockRestore();
    });
});