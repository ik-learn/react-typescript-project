import { render } from "@testing-library/react"
import SnapshotComponent from "../../../src/common/parts/SnapshotComponent";


it ("Snapshotテスト", () => {
   const { container } =  render(<SnapshotComponent text="React"/>);
   expect(container).toMatchSnapshot();
});