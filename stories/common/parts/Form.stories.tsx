import Form from "../../../src/common/parts/Form";
import type { Meta, StoryObj } from "@storybook/react";
import { within , userEvent } from "@storybook/testing-library";
import { expect } from "@storybook/jest";


const meta = {
    title: "Form",
    component: Form
} as Meta<typeof Form>


export default meta;

type Story = StoryObj<typeof Form>;

export const Default: Story = {};

export const Testing: Story = {
    play: async ({ canvasElement })  => {
        // screenと類似
        const canvas = within(canvasElement);

        // 入力フォームの要素取得
        const input = canvas.getByRole("textbox");
        // 入力フォーム初期値検証
        await expect(input).toHaveTextContent("");

        // ユーザー入力した設定
        await userEvent.type(input, "play function");
        // ユーザー入力した検証
        await expect(canvas.getByDisplayValue("play function")).toBeInTheDocument();
    }
}