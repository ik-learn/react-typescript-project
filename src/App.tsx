import './common/css/common.css'
import RouterComponent from './router/RouterComponent'

function App() {

  return (
    <RouterComponent/>
  )
}

export default App
