const NotFoundPage = () => {
    return (
        <div
            style={{
                background: "#000",
                color: "#F00"
            }}
        >
            Not Found
        </div>
    );
}
export default NotFoundPage;