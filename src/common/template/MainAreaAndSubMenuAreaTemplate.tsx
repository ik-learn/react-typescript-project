export type MainAreaAndSubMenuProps = {
    title: string;
    mainArea: JSX.Element;
    subMenuArea: JSX.Element;
}

/**
 * メインエリアとサブメニューエリアのTemplate
 * 
 * @param props メインエリアとサブメニューエリアのコンポーネント
 * @returns メインエリアとサブメニューエリアのTemplate
 */
const MainAreaAndSubMenuTemplate = (props: MainAreaAndSubMenuProps) => {

    const {title, mainArea, subMenuArea} = props;

    return (
        <div>
            <div
                style={{
                    display:"block",
                    width:"30%"
                }}
            >
                <div
                    style={{
                        display:"block",
                        height: "30%"
                    }}
                >
                    {title}
                </div>
                <div
                    style={{
                        display:"block",
                        height: "70%"
                    }}
                >
                    {subMenuArea}
                </div>            
            </div>
            <div
                style={{
                    display:"block",
                    width:"70%"
                }}
            >
                {mainArea}
            </div>
        </div>
    );
}
export default MainAreaAndSubMenuTemplate;