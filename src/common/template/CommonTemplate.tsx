export type CommonTemplateProps = {
    headerArea?: JSX.Element
    clientArea?: JSX.Element
    footerArea?: JSX.Element
}

/**
 * 共通Template
 * 
 * @param props  
 * @returns 共通Template(ヘッダー部, フッター部)
 */
const CommonTemplate = (props: CommonTemplateProps) => {

    const {headerArea, clientArea, footerArea} = props;

    return (
        <div>
            <div
                className="common-header"
            >
                {headerArea ?? headerArea}
            </div>
            <div
                className="commom-body"
            >
                {clientArea ?? clientArea}
            </div>
            <div
                className="commom-footer"
            >
                {footerArea ?? footerArea}
            </div>
        </div>
    );
}
export default CommonTemplate;