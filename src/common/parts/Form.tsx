import { FormEvent, useState } from 'react'

const Form = () => {

 const [inputValue, setInputValue] = useState(""); 

  const onSubmitEvent = (event: FormEvent) => {
    event.preventDefault();
    alert(`submitted: ${inputValue}`);
  }

  return (
    <form onSubmit={onSubmitEvent}>
        <input
            type="text"
            value={inputValue}
            onChange={(e) => setInputValue(e.target.value)}
            placeholder="Enter text"
        />
        <button type='submit'>送信</button>
    </form>
  )
}

export default Form;