import { useState } from 'react'

function AsyncComponent() {
  
  const [text, setText] = useState("Initial text");

  const onClickedEvent = async () => {
    setText("Loading...");
    setTimeout(() => {
        setText("Updated text");
    }, 2000);
  }

  return (
    <div>
        <button onClick={onClickedEvent}>Update Text</button>
        <p>{text}</p>
    </div>
  );
};

export default AsyncComponent