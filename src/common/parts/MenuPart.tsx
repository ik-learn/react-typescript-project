import { Button } from "@mui/material";
import { Link, useNavigate } from "react-router-dom";
import { URL_DRAG_AND_DROP, URL_HOME, URL_STEP } from "../consts/UrlConst";


const MenuPart = () => {

    const navigate = useNavigate();

    const onClickBtnEvent = (url: string) => {
        navigate(url);
    }


    return (
        <div
            style={{
                height: "100%",
                display: "flex",
                verticalAlign: "center"
            }}
        >
            <div
                className="menu"
                style={{
                    
                }}
            >
                <Button
                    sx={{
                      height: "20px"
                    }}
                    onClick={() => onClickBtnEvent(URL_HOME)}
                >
                    HOME画面
                </Button>
            </div>
            <div
                className="menu"
            >
                <Button
                    sx={{
                      height: "20px"
                    }}
                    onClick={() => onClickBtnEvent(URL_STEP)}
                >
                    パンくず画面
                </Button>
            </div>
            <div
                className="menu"
            >
                <Button
                    sx={{
                      height: "20px"
                    }}
                    onClick={() => onClickBtnEvent(URL_DRAG_AND_DROP)}
                >
                    ドラッグ&ドロップ画面
                </Button>
            </div>
        </div>
    );

}

export default MenuPart;