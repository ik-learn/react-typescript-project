import React from 'react'

type SnapshotProps = {
    text: string;
}


const SnapshotComponent: React.FC<SnapshotProps> = ({text}) => {
  return (
    <div>
        <p>
            {text}
        </p>
    </div>
  )
}

export default SnapshotComponent;
