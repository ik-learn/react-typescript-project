import { useState } from 'react'
import axios from 'axios';

interface User {
    id: number;
    name: string;
}

export const UserSearch = () => {

    // 検索条件
    const [query, setQuery] = useState("");
    
    // ユーザ情報
    const [user, setUser] = useState<User | null>(null);

    const onClickedSearchBtnEvent = async () => {
        const { data } = await axios.get<User>(`/api/users?query=${query}`)
        setUser(data);
    };

    return (
        <div>
            <input type="text" value={query} onChange={(e) => setQuery(e.target.value)}/>
            <button onClick={onClickedSearchBtnEvent}>Search</button>
            {user && <div>{user.name}</div>}
        </div>
    )
}

export default UserSearch
