import { MENU_TITLE_LIST } from "../consts/TitleConst";
import { getWidthByTitleLength } from "../utils/StringUtils";
import MenuPart from "./MenuPart";

type HeaderType = {
    headerTitle: string;
}

const HeaderArea = (props: HeaderType) => {

    const getMenuWidth = () => {
        let menuWidth : number = 0;
        MENU_TITLE_LIST?.forEach(menuTitle => menuWidth+= getWidthByTitleLength(menuTitle));
        return menuWidth;
    }


    const {
        headerTitle
    } = props;


    return (
        <div
            style={{
                display:"flex",
                left: "0px",
                top: "0px"
            }}
        >
            <div
                style={{
                    fontSize: "18px",
                    color: "white",
                    width: getWidthByTitleLength(headerTitle)
                }}
            >
                {headerTitle}
            </div>
            <div
                style={{
                    display: "flex",
                    marginLeft: "10px",
                    marginTop: "23px",
                    verticalAlign: "center",
                    width: getMenuWidth()
                }}
            >
                {<MenuPart/>}
            </div>
        </div>
    );
}
export default HeaderArea;