import axios from "axios"

/**
 * Rest通信(Get)
 * 
 * @param url アクセス先のURL
 */
export async function getFetch(
        url: string, 
        functionWhenSuccess: any, 
        functionWhenException: any) {
    await axios.get(url)
    .then((data) => {
        functionWhenSuccess(data);
    })
    .catch((exception) => {
        functionWhenException(exception);
    });
}

/**
 * Rest通信(post)
 * 
 * @param url アクセス先のURL
 * @param param  送信データ
 */
export async function postFetch(url: string, param: any) {
    await axios.post(url, param)
    .then((data) => {
        return JSON.stringify(data);
    })
    .catch((exception) => {
        return JSON.stringify(exception);
    });
}