export const MENU_TITLE_HOME = "HOME画面"
export const MENU_TITLE_STEPPER = "パンくず画面"
export const MENU_TITLE_DRAG_AND_DROP = "ドラッグ&ドロップ画面"

// メニューリスト
export const MENU_TITLE_LIST = [MENU_TITLE_HOME, MENU_TITLE_STEPPER, MENU_TITLE_DRAG_AND_DROP];
