// 1文字辺りの横幅
const CHAR_UNIT_WIDTH = 18;
// タイトル外の余分幅
const TITLE_MARGIN = 30;

/**
 * タイトル文字数による横幅取得
 * 
 * <pre>
 * タイトル文字数により横幅を計算する
 * </pre>
 * 
 * @param {string} title タイトル
 * @returns 横幅
 */
export const getWidthByTitleLength = (title: string) => {
    // タイトルの文字数
    let titleLength = title.length;

    // タイトルの文字数が0の場合
    if (titleLength === 0) {
        return titleLength;
    }

    //  タイトル横幅
    let titleWidth = titleLength * CHAR_UNIT_WIDTH + TITLE_MARGIN;
    return titleWidth;
}