/**
 * 
 * @param list 
 * @param target 
 * @returns 
 */
export const find = (list: string[] , target: string ) => {
    return list.find(string => string === target);
}

/**
 * 
 * @param list 
 * @param target 
 * @returns 
 */
export const isExistToList = (list: string[], target: string) => {
    return list.includes(target);
}

