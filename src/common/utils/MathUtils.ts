export function summary(
    a?:number, 
    b?:number) {

    if (a && b) {
        return a + b;
    } else {
        return "error";
    }
}

export function subtraction(
    a:number | null, 
    b:number | null) {

    if (a && b) {
        return a - b;
    } else {
        return "error";
    }
}

export function multiplication(
    a:number | null, 
    b:number | null) {

    if (a && b) {
        return a * b;
    } else {
        return "error";
    }
}

export function division(
    a:number | null, 
    b:number | null) {

    if (a && b) {
        return a / b;
    } else {
        return "error";
    }
}

