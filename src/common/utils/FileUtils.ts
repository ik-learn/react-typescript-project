import fs from 'fs';
// import util  from "util";

// データタイプ:JSON
const DATA_TYPE_JSON = "json";

/**
 * ファイル読み込み処理
 * 
 * @param {string} fileName ファイル名
 * @param {string} dataType ファイルデータ出力タイプ
 */
export async function readFile (fileName: string, dataType?: string) {
    await new Promise(() => fs.readFileSync(fileName))
    // 正常終了処理
    .then((data) => {
        // 指定したデータタイプがJSONの場合
        if (dataType && dataType === DATA_TYPE_JSON) {
            return JSON.stringify(data);
        } else {
            return String(data);
        }
    })
    // 異常終了処理
    .catch((exception) => {
        return String(exception);
    });
}