import { STEP_TITEL_DETAIL1 } from "../../const/StepperConst";

const step2 = () => {
  return (
    <div
      style={{
        textAlign: "left"
      }}  
    >
        <div>
            <span>タイトル: {STEP_TITEL_DETAIL1}</span>
        </div>
     </div>
  )
}

export default step2;