import { STEP_TITEL_BASE, STEP_TITEL_CONFIRM, STEP_TITEL_DETAIL1, STEP_TITEL_DETAIL2 } from "../../const/StepperConst";

const Step4 = () => {

    return (
        <div
            style={{
                textAlign: "left"
            }}
        >
            <div>
                <span>
                    タイトル:{STEP_TITEL_CONFIRM}
                </span>
            </div>
            <div
                style={{
                    marginLeft: "18px"
                }}
            >
                <li>
                    {STEP_TITEL_BASE}
                </li>
            </div>
            <div
                style={{
                    marginLeft: "18px"
                }}    
            >
                <li>
                    {STEP_TITEL_DETAIL1}
                </li>
            </div>
            <div
                style={{
                    marginLeft: "18px"
                }}    
            >
                <li>
                    {STEP_TITEL_DETAIL2}
                </li>
            </div>
        </div>
    );
}

export default Step4;