import { STEP_TITEL_DETAIL2 } from "../../const/StepperConst";

const Step3 = () => {
    return (
        <div
            style={{
                textAlign: "left"
            }}        
        >
            <div>
                <span>タイトル: {STEP_TITEL_DETAIL2}</span>
            </div>
        </div>
    );
}

export default Step3;