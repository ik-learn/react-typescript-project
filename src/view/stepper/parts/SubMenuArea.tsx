import * as React from 'react';
import Box from '@mui/material/Box';
import Stepper from '@mui/material/Stepper';
import Step from '@mui/material/Step';
import StepLabel from '@mui/material/StepLabel';
import StepContent from '@mui/material/StepContent';
import Button from '@mui/material/Button';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import { steps } from '../data/StepData';

type verticalStep = {
    activeStep: number;
    setActiveStep: any;
    
}
const SubMenuArea = (props: verticalStep) => {

  const {activeStep, setActiveStep} = props;
  
  const onClickedStep = (selectStepId: number) => {
    setActiveStep(selectStepId);
  }

  return (
    <Box sx={{ maxWidth: 250 }}>
      <Stepper 
        activeStep={activeStep} 
        orientation="vertical">
            {steps.map((step) => (
            <Step 
                key={step.stepId}
                onClick={() => onClickedStep(step.stepId)}>
                <StepLabel>
                    {step.label}
                </StepLabel>
            </Step>
            ))}
      </Stepper>
    </Box>
  );
}

export default SubMenuArea;