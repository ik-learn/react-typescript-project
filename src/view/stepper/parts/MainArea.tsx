
import { STEP_FOUR, STEP_ONE, STEP_THREE, STEP_TWO } from "../const/StepperConst";
import Step1 from "./step/Step1";
import Step2 from "./step/Step2";
import Step3 from "./step/Step3";
import Step4 from "./step/Step4";

type MainAreaProps = {
    selectStep : string | number;
}

const MainArea = (props: MainAreaProps) => {
    const { selectStep } = props;
    return (
        <div
            style={{
                height: "640px"
            }}        
        >
           {selectStep == STEP_ONE && <Step1/>}
           {selectStep == STEP_TWO && <Step2/>}
           {selectStep == STEP_THREE && <Step3/>}
           {selectStep == STEP_FOUR && <Step4/>}
        </div>
    );
}

export default MainArea;