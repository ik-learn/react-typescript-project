import { STEP_FOUR, STEP_ONE, STEP_THREE, STEP_TITEL_BASE, STEP_TITEL_CONFIRM, STEP_TITEL_DETAIL1, STEP_TITEL_DETAIL2, STEP_TWO } from "../const/StepperConst";

export const steps = [
    {
        stepId: STEP_ONE,
        label: STEP_TITEL_BASE,
    },
    {
        stepId: STEP_TWO,
        label: STEP_TITEL_DETAIL1,
    },
    {
        stepId: STEP_THREE,
        label: STEP_TITEL_DETAIL2,
    },
    {
        stepId: STEP_FOUR,
        label: STEP_TITEL_CONFIRM
    },
  ];