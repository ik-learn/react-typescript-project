export const STEP_ONE  = 0;
export const STEP_TWO  = 1;
export const STEP_THREE = 2;
export const STEP_FOUR = 3;

export const STEP_TITEL_BASE = "基本情報";
export const STEP_TITEL_DETAIL1 = "詳細情報１";
export const STEP_TITEL_DETAIL2 = "詳細情報２";
export const STEP_TITEL_CONFIRM = "入力確認";   