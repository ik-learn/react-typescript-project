export type StepInfo = {
    // Stepの一意となるキー
    stepId: string 
    // Stepのタイトル
    stperTitle: string
    // Stepの説明
    description: string
}