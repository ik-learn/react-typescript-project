import { useState } from "react";
import MainArea from "../parts/MainArea";
import SubMenuArea from "../parts/SubMenuArea";
import '../assets/stepper.css';
import { STEP_ONE } from "../const/StepperConst";

const StepperTemplate = () => {

    // 選択したStep対象
    const [activeStep, setActiveStep] = useState(STEP_ONE);

    return (
        <div
            style={{
                display: "flex"
            }}
        >
            <div
                className="subMenuArea"
            >   
                <SubMenuArea
                    activeStep={activeStep}
                    setActiveStep={setActiveStep}
                />
            </div>
            <div
                className="mainArea"
            >
                <MainArea
                    selectStep={activeStep}
                />
            </div>
        </div>
    );
}

export default StepperTemplate;