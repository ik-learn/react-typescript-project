/**
 * Stepper押下時イベント
 * 
 * @param stepId 
 * @param setSelectStep 
 */
export const onClieckedStep = (
        stepId: string | null | undefined, 
        setSelectStep: React.Dispatch<React.SetStateAction<string>>
    ) => {
    if (setSelectStep && stepId) {
        setSelectStep(stepId);
    }
}


/**
 * 選択対象のパンくず判定処理
 * 
 * @param stepId 
 * @param selectedSetepId 
 * @returns 
 */
export const isSelectedStep = (stepId: string, selectedSetepId: string) => {
    return stepId === selectedSetepId;
}
