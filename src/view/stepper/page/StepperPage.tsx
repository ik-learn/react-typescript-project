import { MENU_TITLE_STEPPER } from "../../../common/consts/TitleConst";
import FooterArea from "../../../common/parts/FooterArea";
import HeaderArea from "../../../common/parts/HeaderArea";
import CommonTemplate from "../../../common/template/CommonTemplate";
import StepperTemplate from "../template/StepperTemplate";

const StepperPage = () => {
    return (
        <CommonTemplate
            headerArea={
                <HeaderArea
                    headerTitle={MENU_TITLE_STEPPER}
                />
            }
            clientArea={
                <StepperTemplate/>
            }
            footerArea={<FooterArea/>}
        />
    );

}
export default StepperPage;