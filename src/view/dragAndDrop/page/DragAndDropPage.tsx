import { MENU_TITLE_DRAG_AND_DROP } from "../../../common/consts/TitleConst";
import FooterArea from "../../../common/parts/FooterArea";
import HeaderArea from "../../../common/parts/HeaderArea";
import CommonTemplate from "../../../common/template/CommonTemplate";

const DragAndDroPage = () => {
    return (
        <CommonTemplate 
        headerArea={
            <HeaderArea
                headerTitle={MENU_TITLE_DRAG_AND_DROP}
            />
        }
        clientArea={
            <div>
            ドラッグ&ドロップ画面
            </div>
        }
        footerArea={
            <FooterArea/>
        }
        />
    );
}

export default DragAndDroPage;