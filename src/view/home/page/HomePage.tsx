import { MENU_TITLE_HOME } from "../../../common/consts/TitleConst";
import HeaderArea from "../../../common/parts/HeaderArea";
import CommonTemplate from "../../../common/template/CommonTemplate";
import HomePageClientArea from "../parts/HomePageClientArea";

const HomePage = () => {
    return (
        <div>
            <CommonTemplate
                headerArea={
                    <HeaderArea
                        headerTitle={MENU_TITLE_HOME}
                    />
                }
                clientArea={
                    <HomePageClientArea/>
                } 
            />
        </div>
    );
}
export default HomePage;