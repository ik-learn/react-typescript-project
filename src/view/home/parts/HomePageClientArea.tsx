import { Link } from "react-router-dom";
import { URL_DRAG_AND_DROP, URL_HOME, URL_STEP } from "../../../common/consts/UrlConst";
import { MENU_TITLE_DRAG_AND_DROP, MENU_TITLE_HOME, MENU_TITLE_STEPPER } from "../../../common/consts/TitleConst";
const HomePageClientArea = () => {
    return (
        <div
            style={{
                color: "#000",
                width: "1920px"
            }}
        >
            <ul>
                <li>
                    <Link to={URL_HOME}>{MENU_TITLE_HOME}</Link>
                </li>            
                <li>
                    <Link to={URL_STEP}>{MENU_TITLE_STEPPER}</Link>
                </li>                
                <li>
                    <Link to={URL_DRAG_AND_DROP}>{MENU_TITLE_DRAG_AND_DROP}</Link>
                </li>
            </ul>
        </div>
    );
}

export default HomePageClientArea;