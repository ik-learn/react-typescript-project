import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "../view/home/page/HomePage";
import StepperPage from "../view/stepper/page/StepperPage";
import DragAndDropPage from "../view/dragAndDrop/page/DragAndDropPage";
import NotFoundPage from "../error/NotFoundPage";
import { URL_DEFAULT, URL_DRAG_AND_DROP, URL_HOME, URL_STEP } from "../common/consts/UrlConst";

const RouterComponent = () => {
    return (
        <BrowserRouter>
        <Routes>
          <Route path={URL_DEFAULT} element={<HomePage/>} />
          <Route path={URL_HOME} element={<HomePage/>} />
          <Route path={URL_STEP} element={<StepperPage/>} />
          <Route path={URL_DRAG_AND_DROP} element={<DragAndDropPage/>} />
          <Route path="*" element={<NotFoundPage/>} />
        </Routes>
      </BrowserRouter>  
    );
}

export default RouterComponent;